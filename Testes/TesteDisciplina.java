package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import códigos.Aluno;
import códigos.ControleDisciplina;
import códigos.Disciplina;

public class TesteDisciplina {
	public TesteDisciplina(){
		
	}

	private Disciplina disciplinaTeste;
	private ControleDisciplina controleDisciplinaTeste;
	
	@Before
	public void setUp() throws Exception{
		
	}
	@Test
	public void testNomeDisciplina() {
		disciplinaTeste = new Disciplina();
		disciplinaTeste.setNomeDisciplina("Calculo");
		assertEquals(disciplinaTeste.getNomeDisciplina(), "Calculo");
	}

	@Test
	public void testCodigo() {
		disciplinaTeste = new Disciplina();
		disciplinaTeste.setCodigo("500");
		assertEquals(disciplinaTeste.getCodigo(), "500");
	}
}
