package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import códigos.Aluno;

public class TesteAluno {
	public TesteAluno(){
		
	}
	
	private Aluno alunoTeste;
	@Before
	public void setUp() throws Exception{
	}

	@Test
	public void testNome() {
		alunoTeste = new Aluno();
		alunoTeste.setNome("Rafael");
		assertEquals(alunoTeste.getNome(), "Rafael");
	}

	@Test
	public void testMatricula() {
		alunoTeste = new Aluno();
		alunoTeste.setMatricula("12345");
		assertEquals(alunoTeste.getMatricula(), "12345");
	}
}
