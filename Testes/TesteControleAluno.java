package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import códigos.Aluno;
import códigos.ControleAluno;

public class TesteControleAluno {
	public TesteControleAluno(){
		
	}
	private Aluno alunoTeste;
	private ControleAluno controleTeste;
	
	@Before
	public void setUp() throws Exception{
	}

	@Test
	public void testAdicionar() {
		
		alunoTeste = new Aluno();
		alunoTeste.setNome("Hugo");
		controleTeste.adicionar(alunoTeste);
		assertEquals(controleTeste.adicionar(alunoTeste), "Aluno adicionado com Sucesso!");
	
		
		}
	@Test
	public void testRemover() {
		
		alunoTeste = new Aluno();
		alunoTeste.setNome("Hugo");
		controleTeste.remover(alunoTeste);
		assertEquals(controleTeste.adicionar(alunoTeste), "Aluno removido com Sucesso!");
	
		
		}
	
	@Test
	public void testPesquisarAluno() {
		
		alunoTeste = new Aluno();
		alunoTeste.setNome("Hugo");
		controleTeste.adicionar(alunoTeste);
		controleTeste.pesquisarAluno("Hugo");
		assertEquals(controleTeste.pesquisarAluno("Hugo"), "Hugo");
	
		
		}

}
