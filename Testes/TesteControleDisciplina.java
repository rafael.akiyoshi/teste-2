package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import códigos.ControleDisciplina;
import códigos.Disciplina;

public class TesteControleDisciplina {
	public TesteControleDisciplina(){
		
	}
	
	private Disciplina disciplinaTeste;
	private ControleDisciplina controleDisciplinaTeste;
	
	
	@Test
	public void testAdicionar() {
		
		disciplinaTeste = new Disciplina();
		disciplinaTeste.setNomeDisciplina("Calculo");
		
		controleDisciplinaTeste = new ControleDisciplina();
		controleDisciplinaTeste.adicionar(disciplinaTeste);
		
		assertEquals(1,controleDisciplinaTeste.getListaDisciplinas().size());
	}
	@Test
	public void testRemover() {
		
		disciplinaTeste = new Disciplina();
		disciplinaTeste.setNomeDisciplina("Calculo");
		
		controleDisciplinaTeste = new ControleDisciplina();
		controleDisciplinaTeste.adicionar(disciplinaTeste);
		controleDisciplinaTeste.remover(disciplinaTeste);
		
		assertEquals(0,controleDisciplinaTeste.getListaDisciplinas().size());
	}
	@Test
	public void testPesquisar() {
		
		disciplinaTeste = new Disciplina();
		disciplinaTeste.setNomeDisciplina("Calculo");
		
		controleDisciplinaTeste = new ControleDisciplina();
		controleDisciplinaTeste.adicionar(disciplinaTeste);
		controleDisciplinaTeste.pesquisarNomeDisciplina("Calculo");
		assertEquals(("Hugo"), "Hugo");
	}
	
}
