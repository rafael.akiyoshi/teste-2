package códigos;

import java.io.*;
import java.util.ArrayList;
public class CadastroAlunoDisciplina{

	public static void main(String[] args) throws IOException{

	InputStream entradaSistema = System.in;	
	InputStreamReader leitor = new InputStreamReader(entradaSistema);
	BufferedReader leitorEntrada = new BufferedReader(leitor);
	String entradaTeclado;
	char menuOpcao;

	ControleAluno umControle = new ControleAluno();
	Aluno umAluno = new Aluno();
	String nomeAluno;

	ControleDisciplina umControleDisciplina = new ControleDisciplina();
	Disciplina umaDisciplina = new Disciplina();
	String nomeDisciplina;

	do{
			System.out.println("=======MENU======");
			System.out.println("1) Adicionar ALuno");
			System.out.println("2) Remover Aluno");
			System.out.println("3) Pesquisar Aluno");
			
			System.out.println("4) Adicionar Disciplina");
			System.out.println("5) Remover Disciplina");
			System.out.println("6) Pesquisar Disciplina");
			System.out.println("Digite a opcao desejada");
			entradaTeclado = leitorEntrada.readLine();
			menuOpcao = entradaTeclado.charAt(0);

			switch (menuOpcao){
			
			case '1':
				System.out.println("Digite o nome do aluno: ");
				entradaTeclado = leitorEntrada.readLine();
				String umNome = entradaTeclado;
				System.out.println("Digite a matricula do aluno: ");
				entradaTeclado = leitorEntrada.readLine();
				String umaMatricula = entradaTeclado;
				Aluno umAluno1 = new Aluno(umNome, umaMatricula);
				String mensagemAdicionar = umControle.adicionar(umAluno1);
				System.out.println(mensagemAdicionar);
				System.out.println("Deseja adicionar Disciplinas ao aluno?");
				System.out.println("1) Sim");
				System.out.println("2) Nao");
				String disciplinaOpcao;
				do {
					entradaTeclado = leitorEntrada.readLine();
					disciplinaOpcao = entradaTeclado;
					if (disciplinaOpcao.equals("1")) {
						System.out.println("Digite o nome da Disciplina");
						entradaTeclado = leitorEntrada.readLine();
						String umaDisciplina1 = entradaTeclado;
						Disciplina materia = new Disciplina();
						materia = umControleDisciplina
								.pesquisarNomeDisciplina(umaDisciplina1);
						if (materia != null) {
							umControleDisciplina.adicionarAlunoEmDisciplina(
									umAluno1, materia);
						} else {
							System.out.println("Disciplina Inexistente");
							disciplinaOpcao = "2";
						}
					}
				} while (disciplinaOpcao.equals("1"));
				break;

			case '2':
				System.out.println("Digite o nome do aluno: ");
				entradaTeclado = leitorEntrada.readLine();
				String umNome1 = entradaTeclado;
				umAluno1 = umControle.pesquisarAluno(umNome1);
				umControle.remover(umAluno1);
				String mensagemRemover = umControle.remover(umAluno1);
				System.out.println(mensagemRemover);
				break;

			case'3':
				System.out.println("Digite o nome do aluno: ");
				entradaTeclado = leitorEntrada.readLine();
				String umNome11 = entradaTeclado;
				umAluno1 = umControle.pesquisarAluno(umNome11);
				System.out.println("Nome: " + umAluno1.getNome());
				System.out.println("Matricula: " + umAluno1.getMatricula());
				break;

			case'4':
				System.out.println("Digite o nome da Disciplina: ");
				entradaTeclado = leitorEntrada.readLine();
				String umNomeDisciplina = entradaTeclado;
				System.out.println("Digite o código da Disciplina: ");
				entradaTeclado = leitorEntrada.readLine();
				String umCodigo = entradaTeclado;
				Disciplina umaDisciplina1 = new Disciplina(umNomeDisciplina , umCodigo);
				String mensagem11 = umControleDisciplina.adicionar(umaDisciplina1);
				break;

			case'5':			
				System.out.println("Digite o nome da Disciplina: ");
				entradaTeclado = leitorEntrada.readLine();
				String umNomeDisciplina1 = entradaTeclado;
				umaDisciplina1 = umControleDisciplina.pesquisarNomeDisciplina(umNomeDisciplina1);
				umControleDisciplina.remover(umaDisciplina1);
				String mensagem111 = umControleDisciplina.remover(umaDisciplina1);
				break;

			case'6':
				System.out.println("Digite o nome da disciplina: ");
				entradaTeclado = leitorEntrada.readLine();
				String umNomeDisciplina11 = entradaTeclado;
				umaDisciplina1 = umControleDisciplina.pesquisarNomeDisciplina(umNomeDisciplina11);
				System.out.println("Curso: " + umaDisciplina1.getNomeDisciplina());
				System.out.println("Codigo: " + umaDisciplina1.getCodigo());

				break;
		
			case'0':
				menuOpcao = '0';
				break;

			default:
				System.out.println("Opcao Invalida");
				}
	
		}while(menuOpcao !='0');

	}

}
