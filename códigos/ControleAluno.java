package códigos;

import java.util.*;

public class ControleAluno{
	
	private ArrayList<Aluno> listaAlunos;

	public ControleAluno(){
		listaAlunos = new ArrayList<Aluno>();
	} 

	public String adicionar(Aluno umAluno){
		String mensagemAdicionar = "Aluno adicionado com Sucesso!";
		listaAlunos.add(umAluno);
		return mensagemAdicionar;
	}
	
	public String remover(Aluno umAluno){
		String mensagemRemover = "Aluno removido com Sucesso!";
		listaAlunos.remove(umAluno);
		return mensagemRemover;
	}

	public Aluno pesquisarAluno(String umNomePesquisar){
		for(Aluno umAluno: listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(umNomePesquisar)) return umAluno;
		}
	return null;

	}
}
